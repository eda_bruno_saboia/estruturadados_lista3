package br.ufc.list3.q1

import scala.collection.mutable.ListBuffer

class HashTable (n:Int) extends ITable{
  
  var vector: Array[List[Int]] = new Array[List[Int]](n/2)
  var node : Int = 0


  override def createTable(): Unit ={
    for(i <- 0 until vector.length){
      vector(i) = Nil
    }
  }
  
  override def insertValue(v: Int): Unit ={
    node = hashFunction(v)
    
    vector(node) = v :: vector(node)
    
    println("O node eh: " +node)
    println("O valor de v eh: " +v)
    println("O valor de node eh: " + vector(node))
  }
  
  private def hashFunction(value: Int): Int ={
    value%(n/2)
  }
  
  override def findValue(v: Int): Option[Int] ={
    val node = hashFunction(v)
    var vnode = vector(node)
    vnode.find( _ == v ) 

  }
  
  //ainda nao esta funcionando ok  
  override def removeValue(v: Int): Unit ={
    node = hashFunction(v)
    vector(node) = removeValueOnList(vector(node), v)
  }
  
  private def removeValueOnList(list: List[Int], v: Int): List[Int] = list match {
    case Nil => Nil
    case l :: Nil => if (l == v) Nil else l :: Nil
    case l :: ll => if (l == v) ll else l :: removeValueOnList(ll, v)
    
  } 
  
  override def freeTable(): Unit ={
    vector = null
  }
}

