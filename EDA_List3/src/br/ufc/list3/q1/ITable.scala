package br.ufc.list3.q1

trait ITable {
  def createTable()
  def insertValue(v: Int)
  def findValue(v: Int): Option[Int]
  def removeValue(v: Int)
  def freeTable()
}