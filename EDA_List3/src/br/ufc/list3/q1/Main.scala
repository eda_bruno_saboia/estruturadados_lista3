package br.ufc.list3.q1

object Main {
    def main(args: Array[String]) {
    var hash:HashTable = new HashTable(10)
    
    //cria tabela
    hash.createTable()
    println("Tabela criada")
    
    
    //Insere valores
    hash.insertValue(2)
    hash.insertValue(3)
    hash.insertValue(4)
    
    //Pesquisa o valor
    hash.findValue(2).map(println)
    hash.findValue(3)
    hash.findValue(4)
    
    //Remove o valor
    hash.removeValue(3)
    
    //Pesquisa o valor
    hash.findValue(3).map(println)
  }
}